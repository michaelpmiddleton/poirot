# Poirot
## A Personalized MBTA-CR Companion:  

The MBTA Commuter Rail notifier (Project "Poirot", see Agatha Christie's main detective [_Hercule Poirot_](https://en.wikipedia.org/wiki/Hercule_Poirot) from [_Murder on the Orient Express_](https://en.wikipedia.org/wiki/Murder_on_the_Orient_Express) for the namesake) is a web application that allows users to view live status updates on specific trains of their choosing.

## Contributions &amp; Sources:
Poirot uses the following technologies to function:  

| Service/Contributor | Description |
| :--- | --- |
| [W3.CSS](https://www.w3schools.com/w3css/default.asp) | This is a responsive CSS framework developed by W3 Schools. I use it for a majority of my web-based projects for ease of mobile-web/desktop-web integration. |
| [MBTA API](https://www.mbta.com/developers/v3-api) | All of the data feeds for train information are powered by the MBTA's official API. |


## What's Up with the Branches?  
I'm following the [Gitflow Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) for this project.

| Branch | Purpose |
| :-----: | ------ |
| **master** | Currently live on the website |
| **dev** | Completed features will be merged from their perspective branches into this branch |
| **&lt;feature&gt;** | Individual features that I work on will have their own branches to show the work that has been done on that particular feature |

## Want to Fork?  
Please feel free to use my code as a reference for your own work. With that said, at this time I am requesting that you do not copy this code. Thank you for your understanding and happy hacking!


## Got Questions?  
I'm happy to answer them! Just send me an email!